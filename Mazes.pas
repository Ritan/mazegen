unit Mazes;

interface

//���������: -""- @������ �����
//���������:
//������ DirChances � TMaze, �������� �� ����� �������� ������ �� ����� � ���-� ����-�� @23
//��������� InnerPointsNum: Integer; DirChance: TDirChances � TMaze.Create() @53, 54
//��������� ������ � Step @172-178
//��������� ����� ������ ����� @192-199

uses
   Contnrs, CommonTypes, SysUtils;

Type
   TWorkMode = (Release, Debug, Panic);
   TDirChances = array[1..4] of Byte;

   TMaze = class
   private
      SizeZ, SizeY: Integer;
      Mode: TWorkMode;
      PointList: TCPointList;
      DirChance: TDirChances;
      procedure PrintPointList;  //�������
      Procedure PrintPoint(Point: TCPoint); //�������

   public
      pIn, pOut: TCPoint;
      map: array of array of Integer;
      constructor Create(SizeZ, SizeY: Integer; WorkMode: TWorkMode; InnerPointsNum: Integer; DirChance: TDirChances);
      procedure Generate(InnerPointsNum: Integer = -1);
      procedure Print;
      procedure Refresh;

end;


implementation



constructor TMaze.Create(SizeZ, SizeY: Integer; WorkMode: TWorkMode; InnerPointsNum: Integer; DirChance: TDirChances);
var i: Integer;
begin
   Self.SizeZ:=SizeZ;
   Self.SizeY:=SizeY;
   SetLength(Map, SizeY*2 + 1);
   for i:=0 to SizeY*2 do SetLength(map[i], SizeZ*2 + 1);
   pIn := TCPoint.Create;
   pOut := TCPoint.Create;
   PointList := TCPointList.Create;
   Self.Mode := WorkMode;
   Self.DirChance := DirChance;
   Self.Generate(InnerPointsNum);
end;

procedure TMaze.Generate(InnerPointsNum: Integer = -1);

procedure MapInit;
var z, y: Integer;
begin
         pIn.Y := 1 + 2*Random(SizeY);
         pIn.X := 0;
         pOut.Y := 1 + 2*Random(SizeY);
         pOut.X:=SizeZ*2;
    for z:=0 to SizeZ*2 do
    begin
      map[0,z] := 1;       //���� ID=1
      map[SizeY*2,z] := 2; //��� ID=2
    end;
  for y:=1 to SizeY*2-1 do
    begin
      //��� ������/������� ID=1, ��� � ID=2
      if y<pIn.y then map[y,0] := 1 else
        if y=pIn.y then map[y,0] := 0
          else map[y,0] := 2;
      if y<pOut.y then map[y,SizeZ*2] := 1 else
        if y=pOut.y then map[y,SizeZ*2] := 0
          else map[y,SizeZ*2] := 2;
    end;
    Refresh;
    map[pIn.y, pIn.x]:=0;
    map[pOut.y, pOut.x]:=0;
    refresh;
end;

procedure NewWall(z, y, dz, dy: Integer);
var TmpPoint: TCPoint;
begin
   if (Random > 0.7)and(map[y+dy*2,z+dz*2]=0)and(dz*dy = 0) then
      begin
         //������� �������� � ��� �� �����
         map[y+dy,z+dz] := map[y,z];
         map[y+dy*2,z+dz*2] := map[y,z];
         TmpPoint:= TCPoint.Create(z+2*dz, y+2*dy, dz, dy);
         PointList.Add(TmpPoint);
         TmpPoint.Free;
      end;
end;

procedure FillWall(a,b : Integer);
var i,j: Integer;
begin
   for i:=0 to SizeY*2 do
      for j:=0 to SizeZ*2 do
           if map[i,j] = a then
               map[i,j]:=b;
end;

procedure DoSubstep(Point: TCPoint; dx, dy: Integer; var List: TCPointList);
var i, ddx, ddy, id: Integer;
    P: TCPoint;
begin
  P:=TCPoint.Create(Point.x,Point.y, Point.dx, Point.dy);
           //����� id �� ������� �����

   //��������, ����� ��������� ����� random(4) �� ������� - �������� ������������ ����� ����� �� ���� ������ DoSubstep
   ddx := dx * (Random(4)+1);
   ddy := dy * (Random(4)+1);


  id:=map[p.y, p.x];
  for i:=1 to abs(ddx + ddy) do
    begin

     //��������� �������, ���� ��� �� �����, ����� �������� ����� � ������������ ��� �������
     //if (P.y + dy * 2 > SizeY * 2)or(P.x + dx * 2 > SizeZ * 2) then Exit;


      //���� ����� ����� �� �����, � �� ��������� �� ��������
      //� ������� �����������, ����������� ����
      if (Map[P.y+dy*2,P.x+dx*2]<>0)and((Map[P.y+dy*2,P.x+dx*2]+id) mod 2=1) then
         begin
            P.Free;
            exit;
         end;
      if (Map[P.y+dy*2,P.x+dx*2]=0)or(Map[P.y+dy*2,P.x+dx*2]<id) then
        begin
          Map[P.y+dy,P.x+dx] := id;
          //���� ���� �����������, �� �������� � ���������� ���� ID �� ������
          if Map[P.y+dy*2,P.x+dx*2]<>0 then
            begin
              if id<Map[P.y+dy*2,P.x+dx*2]
                then FillWall(Map[P.y+dy*2,P.x+dx*2],id)
                else FillWall(id,Map[P.y+dy*2,P.x+dx*2]);
            end
          else
            //���� ����� �����, �� ������ ������ �����
            begin
              Map[P.y+dy*2,P.x+dx*2] := id;
              inc(P.y,dy*2);
              inc(P.x,dx*2);
              //�������� ����� - � ����� ������
              List.Add(P);
              PrintPoint(P);
            end;
        end;
    end;
  P.Free;
end;

procedure Step;
var
   i: Integer;
   List: TCPointList;


begin
   List := TCPointList.Create;
   for i:=0 to PointList.Count - 1 do
      begin
        if Random(11) < DirChance[1] then DoSubstep(PointList[i],-1, 0, List);
        //refresh;
        if Random(11) < DirChance[2] then DoSubstep(PointList[i],1, 0, List);
        //refresh;
        if Random(11) < DirChance[3] then DoSubstep(PointList[i],0, -1, List);
        //refresh;
        if Random(11) < DirChance[4] then DoSubstep(PointList[i],0, 1, List);
        //refresh;
      end;
   PointList.Assign(List);
   List.Free;
end;

var
   List: TObjectList;
   i,j: Integer;
   TmpPoint: TCPoint;
begin
MapInit;
//Refresh;
if InnerPointsNum < 0 then j:=sizeZ*SizeY div 5 else j:=InnerPointsNum;
for i:=1 to j do
   begin
      TmpPoint := TCPoint.Create(2 + 2*Random(SizeZ - 2), 2 + 2*Random(SizeY - 2), 0, 0);
      map[TmpPoint.Y, TmpPoint.X]:=Random(j) + 3;
      pointList.Add(TmpPoint);
      TmpPoint.Free;
   end;

for i:=1 to SizeZ do
   begin
     NewWall(i*2, 0, 0, 1);
     NewWall(i*2, SizeY*2, 0, -1);
     //Refresh;
   end;
//PrintPointList;
for i:=1 to SizeY do
   begin
     NewWall(0, i*2, 1, 0);
     NewWall(SizeZ*2, i*2, -1, 0);
     //Refresh;
   end;
//PrintPointList;

while PointList.Count > 0 do
   begin
      Step;
      //PrintPointList;
   end;

end;

procedure TMaze.Print;
var i,j: Integer;
begin
   for i:=0 to SizeY*2 do
      begin
      for j:=0 to SizeZ*2 do
         write(' ',Map[i,j]);
      writeln;
      end;
end;

procedure TMaze.Refresh;
var i: Integer;
begin
  if Mode = Panic then
   begin
      For i:=1 to 20 do writeln;
      Print;
   end;
end;

procedure TMaze.PrintPointList;
var i: Integer;
begin
   if Mode = Panic then
      begin
        for i:=0 to PointList.Count - 1 do
            write(PointList.Items[i].ToString, ',');
      end;  
end;

procedure TMaze.PrintPoint(Point: TCPoint);
begin
   if (Mode = Panic)or(Mode = Debug) then
   Point.Write;
end;

end.
