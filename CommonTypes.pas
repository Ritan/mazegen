unit CommonTypes;


//�������� ��������� TCPointList.Assign, ������ �� ���������� ������������� ������ - ����� ������ �������


interface

uses SysUtils;

type TCPoint = class
  public
      x,y,dx,dy : Integer;
      constructor Create(x: Integer = 0; y: Integer = 0; dx: Integer = 0; dy: Integer = 0);
      procedure Write;     //��� ���������� ���������������� �� �����
      function ToString: string;  //�� �����
  end;


//������ ������� TObjectList'�
//�������� ��� ���������, �� ������ ����� ��������, � �� ��������� �� ���
type TCPointList = class
   private
      list: Array of TCPoint;
      function GetItem(Index: Integer):TCPoint;
      function GetCount: Integer;
   public
      procedure Add(Point: TCPoint);
      procedure Assign(PointList: TCPointList);
      property Items[Index: Integer]:TCPoint read GetItem; default;
      property Count: Integer read GetCount;
   end;
      



implementation

constructor TCPoint.Create(x: Integer = 0; y: Integer = 0; dx: Integer = 0; dy: Integer = 0);
begin
   Self.x:=x;
   Self.y:=y;
   Self.dx:=dx;
   Self.dy:=dy;
end;


//����� ��������� ����� � �������
procedure TCPoint.Write;
begin
  System.Write(ToString);
end;


function TCPoint.ToString: string;
begin
  Result := Format('[%d, %d]', [Self.X, Self.Y]);
end;

procedure TCPointList.Add(Point: TCPoint);
begin
   SetLength(list, Length(list) + 1);
   list[Length(list) - 1]:=TCPoint.Create(Point.x,Point.y, Point.dx, Point.dy);
end;

procedure TCPointList.Assign(PointList: TCPointList);
var i: Integer;
begin
	for i:=0 to Self.Count - 1 do Self.list[i].Free;
   SetLength(list, Length(PointList.List));
   for i:=0 to PointList.Count - 1 do
         List[i] := PointList[i];
end;

function TCPointList.GetItem(Index: Integer):TCPoint;
begin
   Result:=List[Index];
end;

function TCPointList.GetCount: Integer;
begin
   if Assigned(List) then
   Result:=Length(List) else Result:=-1;
end;

end.
