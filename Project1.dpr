program Project1;

{$APPTYPE CONSOLE}
uses
  SysUtils,
  Types,
  Contnrs,
  Mazes in 'Mazes.pas',
  CommonTypes in 'CommonTypes.pas';


var PntNum: Integer;
   SizeZ, SizeY: Integer;
   a: TDirChances;

begin
Randomize;
SizeZ := StrToIntDef(ParamStr(1), Random(100));
SizeY:= StrToIntDef(ParamStr(2), Random(100));
while SizeZ < 10 do SizeZ:= Random(100);
while SizeY < 10 do SizeY:= Random(100);
RandSeed:=StrToIntDef(ParamStr(3), Random(10000));
PntNum:=StrToIntDef(ParamStr(4), 3);
a[1]:=StrToIntDef(ParamStr(5), 5);
a[2]:=StrToIntDef(ParamStr(6), 5);
a[3]:=StrToIntDef(ParamStr(7), 5);
a[4]:=StrToIntDef(ParamStr(8), 5);
   with TMaze.Create(SizeZ, SizeY, Release, PntNum, a) do
      begin
         Assign(output, 'output.txt');
         rewrite(output);
         Print;
         CloseFile(output);
      end;
end.



